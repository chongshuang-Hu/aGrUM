%feature("docstring") gum::GraphicalModel
"
Abstract class for all PGM (associating set of variables and a graph).
"

%feature("docstring") gum::GraphicalModel::names
"
Returns
-------
List[str]
	The names of the graph variables
"

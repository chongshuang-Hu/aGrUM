%feature("docstring") gum::ShaferShenoyMNInference
"
Class used for Shafer-Shenoy inferences for Markov network.

ShaferShenoyInference(bn) -> ShaferShenoyInference
    Parameters:
        * **mn** (*pyAgrum.MarkovNet*) -- a Markov network
"

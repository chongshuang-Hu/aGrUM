Undirected Graphical Model
--------------------------

.. autoclass:: pyAgrum.MarkovNet
			:exclude-members: setProperty, property, propertyWithDefault

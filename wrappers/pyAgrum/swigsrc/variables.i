/**
 *
 *  Copyright 2005-2022 Pierre-Henri WUILLEMIN et Christophe GONZALES (LIP6)
 *   {prenom.nom}_at_lip6.fr
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
%rename ( index ) gum::DiscretizedVariable<double>::index(const double& tick) const;
%ignore **::ticksAsDoubles() const;

%newobject gum::LabelizedVariable::clone();

%extend gum::DiscreteVariable {
  %pythoncode %{
    def __hash__(self):
        return hash(self.name())

    def __getitem__(self,label):   # adding the y() function here
        return self.index(label)
  %}
}

%feature("shadow") gum::LabelizedVariable::addLabel(const std::string& aLabel) %{
def addLabel(self,*args):
    """
    Add a label with a new index (we assume that we will NEVER remove a label).

    Parameters
    ----------
    aLabel : str
        the label to be added to the labelized variable

    Returns
    -------
    pyAgrum.LabelizedVariable
        the labelized variable

    Raises
    ------
      pyAgrum.DuplicateElement
        If the variable already contains the label
    """
    $action(self,*args)
    return self
%}

%feature("shadow") gum::DiscretizedVariable<double>::addTick(const double& aTick) %{
def addTick(self,*args):
    """
    Parameters
    ----------
    aTick : double
        the Tick to be added

    Returns
    -------
    pyAgrum.DiscretizedVariable
        the discretized variable

    Raises
    ------
      pyAgrum.DefaultInLabel
        If the tick is already defined
    """
    $action(self,*args)
    return self
%}

%feature("shadow") gum::IntegerVariable::addValue(int value) %{
def addValue(self,*args):
    """
    Add a value to the list of values for the variable.

    Parameters
    ----------
    value : int
        the new value

    Returns
    -------
    pyAgrum.IntegerVariable
        the Integer variable

    Raises
    ------
      pyAgrum.DuplicateElement
        If the variable already contains the value
    """
    $action(self,*args)
    return self
%}

%extend gum::IntegerVariable {
PyObject *integerDomain() const {
  return PyAgrumHelper::PyListFromSequenceOfInt(self->integerDomain());
}
}
%ignore gum::IntegerVariable::integerDomain;


%feature("shadow") gum::NumericalDiscreteVariable::addValue(double value) %{
def addValue(self,*args):
    """
    Add a value to the list of values for the variable.

    Parameters
    ----------
    value : double
        the new value

    Returns
    -------
    pyAgrum.IntegerVariable
        the Integer variable

    Raises
    ------
      pyAgrum.DuplicateElement
        If the variable already contains the value
    """
    $action(self,*args)
    return self
%}

%extend gum::NumericalDiscreteVariable {
PyObject *numericalDomain() const {
  return PyAgrumHelper::PyListFromSequenceOfDouble(self->numericalDomain());
}
}
%ignore gum::NumericalDiscreteVariable::numericalDomain;
